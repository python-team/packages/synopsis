//
// Copyright (C) 2004 Stefan Seefeld
// All rights reserved.
// Licensed to the public under the terms of the GNU LGPL (>= 2),
// see the file COPYING for details.
//
#ifndef Synopsis_SymbolLookup_hh_
#define Synopsis_SymbolLookup_hh_

#include <Synopsis/SymbolLookup/Symbol.hh>
#include <Synopsis/SymbolLookup/Scope.hh>
#include <Synopsis/SymbolLookup/Scopes.hh>
#include <Synopsis/SymbolLookup/Walker.hh>

#endif
