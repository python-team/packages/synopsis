<sxr filename="Bezier.h">
<line>#ifndef Bezier_h_</line>
<line>#define Bezier_h_</line>
<line></line>
<line>#include "Path.h"</line>
<line>#include &lt;vector&gt;</line>
<line></line>
<line><span class="keyword">namespace</span> <a href="Paths" title="namespace Paths" from="Paths" type="definition">Paths</a></line>
<line>{</line>
<line></line>
<line><span class="comment">/**</span></line>
<line><span class="comment"> * The Bezier class. It implements a Bezier curve</span></line>
<line><span class="comment"> * for the given order.</span></line>
<line><span class="comment"> */</span></line>
<line>template &lt;size_t Order&gt;</line>
<line><span class="keyword">class</span> <a href="Paths::Bezier" title="class Paths::Bezier" from="Paths::Bezier" type="definition">Bezier</a> : <span class="keyword">public</span> <a href="Path" title="class Path" from="Paths::Bezier" type="reference">Path</a></line>
<line>{</line>
<line><span class="keyword">public</span>:</line>
<line>  <span class="comment">/** Create a new Bezier.*/</span></line>
<line>  <a href="Paths::Bezier::Bezier()" title="member function Paths::Bezier::Bezier()" from="Paths::Bezier" type="definition">Bezier</a>();</line>
<line></line>
<line>  <span class="comment">/** @group Manipulators {*/</span></line>
<line></line>
<line>  <span class="comment">/**</span></line>
<line><span class="comment">   * Add a new control point.</span></line>
<line><span class="comment">   * @param p A point</span></line>
<line><span class="comment">   */</span></line>
<line>  <span class="keyword">void</span> <a href="Paths::Bezier::add_control_point(const Vertex&amp;)" title="member function Paths::Bezier::add_control_point(const Vertex&amp;)" from="Paths::Bezier" type="definition">add_control_point</a>(<span class="keyword">const</span> <a href="Vertex" title="struct Vertex" from="Paths::Bezier" type="reference">Vertex</a> &amp;);</line>
<line></line>
<line>  <span class="comment">/**</span></line>
<line><span class="comment">   * Remove the control point at index i.</span></line>
<line><span class="comment">   * @param i An index</span></line>
<line><span class="comment">   */</span></line>
<line>  <span class="keyword">void</span> <a href="Paths::Bezier::remove_control_point(size_t)" title="member function Paths::Bezier::remove_control_point(size_t)" from="Paths::Bezier" type="definition">remove_control_point</a>(<a href="size_t" title="typedef size_t" from="Paths::Bezier" type="reference">size_t</a> i);</line>
<line>  <span class="comment">/** }*/</span></line>
<line>  virtual <span class="keyword">void</span> <a href="Paths::Bezier::draw()" title="member function Paths::Bezier::draw()" from="Paths::Bezier" type="definition">draw</a>();</line>
<line><span class="keyword">private</span>:</line>
<line>  <span class="comment">/** The data...*/</span></line>
<line>  std::<a href="std::vector" title="class std::vector" from="Paths::Bezier" type="reference">vector</a>&lt;<a href="Vertex" title="struct Vertex" from="Paths::Bezier" type="reference">Vertex</a>&gt; <a href="Paths::Bezier::controls_" title="data member Paths::Bezier::controls_" from="Paths::Bezier" type="definition">controls_</a>;</line>
<line>};</line>
<line></line>
<line>}</line>
<line></line>
<line>#endif</line>
</sxr>